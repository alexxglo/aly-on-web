module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/aly-on-web/'
    : '/'
}