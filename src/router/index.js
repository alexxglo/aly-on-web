import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import About from '../views/About.vue'
import Sidebar from '../components/Sidebar.vue'
import MyInfo from '../views/MyInfo.vue'
//import { component } from 'vue/types/umd'
//import { SidebarPlugin } from 'bootstrap-vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/About',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: About
  },
  {
    path:'/Sidebar',
    name: 'Sidebar',
    component: Sidebar
  },
  {
    path: '/MyInfo',
    name: 'MyInfo',
    component: MyInfo
  }

]

const router = new VueRouter({
  routes
})

export default router
